""" Definition of TableRow class, used to store data for car reports """
from datetime import datetime


class TableRow:
    """
    Storiong information representing one row of a table from the Travel journal.
    """

    _date_format = "%Y-%m-%d %H:%M"

    def __init__(
        self,
        ride_start: str,
        readings_start: str,
        address_start: str,
        ride_end: str,
        readings_end: str,
        address_end: str,
        distance: str,
        date_format: str = None,
    ) -> None:
        """
        :arg ride_start: description
        :arg readings_start: description
        :arg address_start: description
        :arg ride_end: description
        :arg readings_end: description
        :arg address_end: description
        :arg distance: description
        """
        self._ride_start = ride_start  # "2023-01-01 01:23"
        self._readings_start = readings_start  # "12345"
        self._address_start = address_start  # "Example street 2"
        self._ride_end = ride_end  # "2023-01-01 02:34"
        self._readings_end = readings_end  # "12350"
        self._address_end = address_end  # "Garden street 456"
        self._distance = distance  # "51.0"
        self.date_format = date_format if date_format else TableRow._date_format

    @property
    def ride_start(self) -> datetime:
        datetime_object = datetime.strptime(self._ride_start, self.date_format)
        return datetime_object

    @property
    def readings_start(self) -> int:
        return int(self._readings_start)

    @property
    def address_start(self) -> str:
        return self._address_start

    @property
    def ride_end(self) -> datetime:
        datetime_object = datetime.strptime(self._ride_end, self.date_format)
        return datetime_object

    @property
    def readings_end(self) -> int:
        return int(self._readings_end)

    @property
    def address_end(self) -> str:
        return self._address_end

    @property
    def distance(self) -> float:
        return float(self._distance)

    def __str__(self) -> str:
        return (
            f"TableRow object\n"
            f"Ride start: {self.ride_start.strftime(self.date_format)}\n"
            f"Readings start: {self.readings_start}\n"
            f"Address start: {self.address_start}\n"
            f"Ride end: {self.ride_end.strftime(self.date_format)}\n"
            f"Readings end: {self.readings_end}\n"
            f"Address end: {self.address_end}\n"
            f"Distance: {self.distance}\n"
        )


class ReportRow:
    """class to store data for a single row of an output report"""

    def __init__(
        self,
        date: str = None,  # "3" or "21"
        ride_start: str = None,  # "10:24"
        ride_end: str = None,  # "10:54"
        readings_start: str = None,  # "12345"
        readings_end: str = None,  # "12350"
        distance: str = None,  # "12.3 km"
        addresses: str = None,  # "Example street 2 - Garden street 456"
    ) -> None:
        self._date: str = date
        self._ride_start: str = ride_start
        self._ride_end: str = ride_end
        self._readings_start: str = readings_start
        self._readings_end: str = readings_end
        self._distance: str = distance
        self._addresses: str = addresses

    @property
    def date(self) -> str:
        return self._date

    @property
    def ride_start(self) -> str:
        return self._ride_start

    @property
    def ride_end(self) -> str:
        return self._ride_end

    @property
    def readings_start(self) -> str:
        return self._readings_start

    @property
    def readings_end(self) -> str:
        return self._readings_end

    @property
    def distance(self) -> str:
        return self._distance

    @property
    def addresses(self) -> str:
        return self._addresses

    @classmethod
    def from_table_row(cls, table_row: TableRow) -> "ReportRow":
        return cls(
            date=table_row.ride_start.strftime("%d"),
            ride_start=table_row.ride_start.strftime("%H:%M"),
            ride_end=table_row.ride_end.strftime("%H:%M"),
            readings_start=str(table_row.readings_start),
            readings_end=str(table_row.readings_end),
            distance=table_row.distance,
            addresses=f"{table_row.address_start} - {table_row.address_end}",
        )

    def __str__(self) -> str:
        return (
            f"ReportRow object\n"
            f"Date: {self.date}\n"
            f"Ride start: {self.ride_start}\n"
            f"Ride end: {self.ride_end}\n"
            f"Readings start: {self.readings_start}\n"
            f"Readings end: {self.readings_end}\n"
            f"Distance: {self.distance}\n"
            f"Addresses: {self.addresses}\n"
        )
