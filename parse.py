# TODO:
# add tests!
# move some code to separate converting functions and separate writing functions.


import csv
from datetime import datetime
import urllib.request
import json
from tablerow import TableRow, ReportRow


class CsvColumnNames:
    category = "Категория"
    start = "Начало"
    readings_start = "Начальные показания (км)"
    address_start = "Начальный адрес"
    end = "Окончание"
    readings_end = "Конечные показания (км)"
    address_end = "Конечный адрес"
    duration = "Длительность"
    distance = "Расстояние (км)"
    designation = "Обозначение"
    user_notes = "Заметки пользователя"


# for start, we could find out how many different months (and of which
# years) are there in a report
def get_months_years(
    csv_file: list[csv.DictReader],
    column: CsvColumnNames = CsvColumnNames(),
) -> datetime:
    """
    Extracts unique year-month combinations from a CSV file containing rides log
    (exported from Volvo app) and return it as a list of datetime objects.

    Args:
        csv_file (list[csv.DictReader]): A list of CSV rows, where each row is a dictionary
            mapping column names to values.

    Returns:
        List[datetime.datetime]: A list of datetime objects, representing all availabe
        month in the CSV file.
    """
    months = []
    for i in range(len(csv_file)):
        date_string = csv_file[i][column.start]
        date_object = datetime.strptime(date_string, "%Y-%m-%d %H:%M")
        year_month: datetime = datetime(date_object.year, date_object.month, 1)
        if year_month in months:
            continue
        months.append(year_month)
    return months


def ask_the_user(months: list[datetime]) -> datetime:
    """
    Ask the user to select one particular month for the car report, from among
    all those included in the records of a CSV file."""
    months = [date.strftime("%B %Y") for date in months]  # Convert to strings
    months_string = ", ".join(months)
    user_input = input(
        f"I detected following months in the CSV file: \n{months_string}\n"
        f"Report for which month do you need? (enter in format 'September 2023')\n> "
    )
    try:
        user_input_dt = datetime.strptime(user_input, "%B %Y")
    except:
        raise ValueError("Wrong input format!")
    return user_input_dt


def bankholidays_api_request(year: datetime) -> dict:
    """
    Sends a request to the boffsaopendata.fi API to retrieve bank holidays
    for a given year.

    Args:
        year (datetime): A datetime object representing the year (year/month
        conbination) for which to retrieve bank holidays.

    Returns:
        dict: A dictionary containing the API response, with bank holidays
        for the given year.
    """
    # transform 'year' argument to str
    year: str = year.strftime("%Y")
    # get API response
    response_dict = {}
    try:
        url = f"https://api.boffsaopendata.fi/bankingcalendar/v1/api/v1/BankHolidays?year={year}&pageNumber=1&pageSize=100"
        req = urllib.request.Request(url)

        with urllib.request.urlopen(req) as response:
            response_dict = json.load(response)
    except Exception as e:
        raise ValueError(e)
    return response_dict


def select_month_records(
    month: datetime,
    csv_file: list[csv.DictReader],
    column: CsvColumnNames = CsvColumnNames(),
) -> list[csv.DictReader]:
    """
    Selects the records from the given CSV file that correspond to the given month.

    Args:
        month (datetime): The month to select records for.
        csv_file (list[csv.DictReader]): The CSV file to select records from.

    Returns:
        list[csv.DictReader]: A list of the selected records.
    """
    records = []
    for i in range(len(csv_file)):
        date_string: str = csv_file[i][column.start]
        date: datetime = datetime.strptime(date_string, "%Y-%m-%d %H:%M")
        if date.month == month.month and date.year == month.year:
            records.append(csv_file[i])
    return records


def get_bank_holidays(
    api_response: dict,
    user_selection: datetime,
) -> list[datetime]:
    """
    Get bank holidays (as datetime.datetime objects) for the given month
    from the API response.
    """
    # Parse the dates from the API response as strings,
    # then convert the dates to datetime objects and filter the dates
    # to only include those that match month with user_selection.month
    filtered_dates = [
        date
        for item in api_response["specialDates"]
        if (date := datetime.strptime(item["date"], "%d.%m.%Y")).month
        == user_selection.month
    ]
    return filtered_dates


def filter_the_records(
    records: list[csv.DictReader],
    holidays: list[datetime],
    remove_pub_holidays: bool = False,
    remove_weekends: bool = True,
    remove_night_rides: bool = True,
    column: CsvColumnNames = CsvColumnNames(),
) -> list[csv.DictReader]:
    """
    Filter out the records to remove holidays, weekends or night rides.
    And also records where distance is 0.
    """
    filtered_records: list[csv.DictReader] = []
    for record in records:
        date_string: str = record[column.start]
        date: datetime = datetime.strptime(date_string, "%Y-%m-%d %H:%M")
        # skip this record if remove_pub_holidays == True and it's a holiday,
        # and is not an empty list
        if remove_pub_holidays and holidays:
            if date in holidays:
                continue
        # skip this record if it's a weekend and remove_weekends == True
        if remove_weekends and date.weekday() >= 5:
            continue
        # skip records for times that fall between 20:00 and 07:00.
        twenty_am = datetime.time(datetime(1900, 1, 1, 20))
        seven_am = datetime.time(datetime(1900, 1, 1, 7))
        if remove_night_rides and (date.time() >= twenty_am or date.time() < seven_am):
            continue
        # if distance is 0, then skip this record too
        if float(record[column.distance]) == 0:
            continue
        filtered_records.append(record)
    return filtered_records


if __name__ == "__main__":
    journal_log = []

    # step 1: read the file
    with open("Journal_log.csv", "r", encoding="utf-8") as file:
        journal_log = csv.DictReader(file)  # read csv
        fieldnames = journal_log.fieldnames  # get column names
        journal_log = list(journal_log)  # convert to list
    months = get_months_years(journal_log)  # get all available months from csv
    user_selection = ask_the_user(months)  # month for which to make a report

    columns = CsvColumnNames()

    if fieldnames and len(fieldnames) == 11:
        columns.category = fieldnames[0]
        columns.start = fieldnames[1]
        columns.readings_start = fieldnames[2]
        columns.address_start = fieldnames[3]
        columns.end = fieldnames[4]
        columns.readings_end = fieldnames[5]
        columns.address_end = fieldnames[6]
        columns.duration = fieldnames[7]
        columns.distance = fieldnames[8]
        columns.designation = fieldnames[9]
        columns.user_notes = fieldnames[10]

    # store all records of that month in a variable
    month_records = select_month_records(user_selection, journal_log)

    # get info about public holidays in finland via some finnish bank API
    api_response: dict = bankholidays_api_request(user_selection)
    bank_holidays: datetime = get_bank_holidays(api_response, user_selection)

    # filter out the records:
    ## remove all the records where date is a weekend
    ## and remove all the records where date is a public holiday
    ## and also records which time is after 20:00 and before 7:00 (night rides)
    month_records = filter_the_records(
        month_records,
        bank_holidays,
        remove_pub_holidays=True,
        remove_weekends=True,
        remove_night_rides=True,
    )

    # sort the records by date and time, ascending
    month_records = sorted(
        month_records,
        key=lambda x: datetime.strptime(x[columns.start], "%Y-%m-%d %H:%M"),
    )

    # convert the records to TableRow objects, store them in a new list
    table_rows = []
    for record in month_records:
        table_row = TableRow(
            record[columns.start],
            record[columns.readings_start],
            record[columns.address_start],
            record[columns.end],
            record[columns.readings_end],
            record[columns.address_end],
            record[columns.distance],
        )
        table_rows.append(table_row)

    # convert the list of TableRow objects to a list of ReportRow objects
    # and store them in a new list
    report_rows = []
    for row in table_rows:
        report_row = ReportRow().from_table_row(row)
        report_rows.append(report_row)

    # write the report to a .csv file
    report_filename = "report.csv"
    with open("report.csv", "w", encoding="utf-8") as csvfile:
        fieldnames = [
            "Date",
            "Ride start",
            "Ride end",
            "Readings start",
            "Readings end",
            "Distance",
            "Addresses",
        ]
        writer = csv.DictWriter(csvfile, fieldnames=fieldnames)
        writer.writeheader()
        for row in report_rows:
            writer.writerow(
                {
                    "Date": row._date,
                    "Ride start": row._ride_start,
                    "Ride end": row._ride_end,
                    "Readings start": row._readings_start,
                    "Readings end": row._readings_end,
                    "Distance": row._distance,
                    "Addresses": row._addresses,
                }
            )

    print(f"Done! Report saved to {report_filename}")

    # example of journal_log[x]:
    # {
    #     'Категория': 'Без обозначения',
    #     'Начало': '2023-05-01 01:23',
    #     'Начальные показания (км)': '12345',
    #     'Начальный адрес': 'Imaginary Street 4',
    #     'Окончание': '2023-05-01 01:45',
    #     'Конечные показания (км)': '12350',
    #     'Конечный адрес': 'Kiitoskatie 2',
    #     'Длительность': '00:11 ч',
    #     'Расстояние (км)': '22.0',
    #     'Обозначение': '',
    #     'Заметки пользователя': ''
    # }
