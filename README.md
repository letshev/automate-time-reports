# Activating a virtual environment

To activate the virtual environment, navigate to the directory containing the .venv folder and run the following command:

## To activate the virtual environment on Unix or Linux:
    source .venv/bin/activate


## To activate the virtual environment on Windows:
    .venv\Scripts\activate.bat

This will activate the virtual environment and allow you to use the packages installed in it.
